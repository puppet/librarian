# Setting up

## Branches and forks
Always work from your own fork of the repos. Fork them to your own namespace and clone them from there to work on stuff. Make your changes in a new branch and submit any changes you've made back to the main repositories in a merge request. Always make sure any YAML files you submit pass yamllint checks and any puppet code passes puppetlint and puppet parser syntax checks. See the `.gitlab-ci.yml` file in each repository for the exact checks that it will need to pass.

## Repository structures

This (librarian) is the base repository, the `manifests`, `modules`, and `hieradata` repos need to be cloned inside this direcotry. If you do not have access to the `hieradata` repository, you can use the `hieradata-testing` repo instead.

After forking each of the projects in https://git.insomnia247.nl/puppet/ (After you log in and set up your ssh keys, click the project, then click "Fork".) you should clone them like so;
```
cd ~/my_projects_workspace
git clone ssh://git@git.insomnia247.nl:33/[YOUR_USER]/librarian.git puppet
cd puppet
git clone ssh://git@git.insomnia247.nl:33/[YOUR_USER]/manifests.git
git clone ssh://git@git.insomnia247.nl:33/[YOUR_USER]/modules.git
```
Then depending on if you're using the hieradata or the hieradata-testing repo;
```
git clone ssh://git@git.insomnia247.nl:33/[YOUR_USER]/hieradata.git
```
or
```
git clone ssh://git@git.insomnia247.nl:33/[YOUR_USER]/hieradata-testing.git hieradata
```

Finally, to ensure our puppet install can find our hieradata you need to create the file "hiera.yaml". We use the following config
```
---
version: 5
defaults:
  datadir: hieradata
  data_hash: yaml_data
hierarchy:
  - name: "YAML hierarchy levels"
    paths:
      - "node/%{facts.fqdn}.yaml"
      - "domain/%{facts.domain}.yaml"
      - "osfamily/%{facts.os.family}.yaml"
      - "common.yaml"
```

Paste this into your hiera.yaml file and your hieradata structure should be compatible with ours.

# Testing

Vagrant and Virtualbox are used for testing changes. Changes are tested on the debian/bullseye64 image.

## Setting up a new vagrant box

If you are already using vagrant and know how to configure a box, you can skip this.

First make sure you have both virtualbox and vagrant installed of course, then to create a new vagrant box run the following commands
```
vagrant init debian/bullseye64
vagrant up
```
This will set up a new Vagrantfile and download the box for you. Once this is done run `vagrant halt` to stop the VM again, since we need to make some tweaks to the Vagrantfile in order to make it work with puppet. If your Vagrantfile was newly generated by the vagrant init command you can use this one instead, otherwise make sure you add any missing config options to your existing Vagrantfile.

```
Vagrant.configure(2) do |config|
  config.vm.box = "debian/bullseye64"
  config.vm.synced_folder "hieradata", "/tmp/vagrant-puppet/hieradata"
  config.vm.provision "puppet", :options => ["--yamldir /hieradata"] do |puppet|
    puppet.hiera_config_path = "hiera.yaml"
    puppet.working_directory = "/tmp/vagrant-puppet"
    puppet.module_path       = "modules"
    puppet.options           = "--verbose"
  end
end
```

If you do not have a default.pp manifest, create a blank one now.
```
touch manifests/default.pp
```

Now lets test our vagrant box. Run `vagrant up` from your terminal and make sure you are in the same directory as you Vagrantfile and the base of the puppet repo. If all goes well it should boot in a few seconds.

Now we'll connect to the VM over ssh. Use the `vagrant ssh` command, this should drop you into an ssh session inside the VM.

While we're in here, there is one more thing to configure. Since the Vagrant user uses uid 1000 by default, this may conflict with any users we want to provision using puppet so we will change the UID of vagrant to 500 instead. Since this is just a VM we don't care about doing it all neat and clean so we can just edit the `/etc/passwd` and `/etc/group` files but before anything else we need to make sure we'll still have access to our SSH key when the new UID is set.
```
sudo chown -R 500:500 /home/vagrant
```

Now we can change the GID and UID
```
sudo sed -i 's/1000/500/g' /etc/group
sudo sed -i 's/1000/500/g' /etc/passwd
```

Now we need to log out and `vagrant ssh` back in to see if our changes worked. Once you log back in and run the `id` command you should see you now have uid 500. Take another snapshot of the VM here just to be safe.

All that is left to do now is to install puppet inside of the VM so we can provision stuff in there. We'll download and install the latest Puppet 5 repo package and use that to install puppet.
```
wget https://apt.puppetlabs.com/puppet-release-bullseye.deb
sudo dpkg -i puppet-release-bullseye.deb
sudo apt update
sudo apt install puppet-agent
```

GREAT! That's finally all we need to do to start testing stuff in vagrant. This would be a good time to go into the Virtualbox GUI and make a quick snapshot of the box so we never have to do all that again.

## Testing puppet code inside vagrant

Now that we've spent an hour setting up a vagrant box, let's test some code! Run `vagrant up` to start your VM, then run `vagrant provision` to have puppet provision the VM. Exciting! But it doesn't seem to do much yet. This is because we've not told it to do anything yet so we'll include a profile. Edit the "manifests/default.pp" file to `include ::role::something` or `::profile::something` you are working on. You can try `include ::role::base` to test right now. Run `vagrant provision` again and you should see puppet making changes for you. From this point onward you should be ready to write and test your puppet code!

If you are missing any of the standard puppet libraries, you can install them using librarian-puppet
```
librarian-puppet install
```

## Notes on testing the lydia6 role

This role relies on some configurations to be present for the hardware that are normally set up in the PXE boot and install for the real hardware. In order to use this profile you will need to;

### Add an extra disk to your virtualbox VM
Mount the disk inside the VM as `/coldstorage`. If you do not want to go trough the trouble, you can also just create a folder called `/coldstorage`, but filesystem quotas will then spill over into the coldstorage drive so only do this if you do not need to test the functioning of the quota system.

### Set the quota mount options for the root disk
The filesystem mount options are also set during the initual setup on the real hardware, so in order to test with this profile we need to manually enable quotas in the VM. For your root disk add the options `jqfmt=vfsv0,grpjquota=aquota.group` to the root partition mount line in `/etc/fstab`. Either reboot or run `sudo mount -o remount /` in the VM to enable the new options.

### Some fake users
Since you will not be able to access the real LDAP server to pull in the user accounts, you will need to manually add some users to the system to test with, or test with the built-in vagrant user if you want to. Just take note that if you break config for the vagrant user during testing you may break the login to the box and might have to restore a snapshot. If you want a working apache config, you will also have to add a "-www" variant of your test user account.

### Power-users group
Some firewall rules will fail to generate if the power-users group does not exist. This group is normally provided by LDAP but on your test setup you will need to `sudo addgroup power-users` by hand.
